## Setting Up Secrets

To simplify the process of creating the required secret files (`secrets.json` and `secrets.tfvars`), we've provided a setup script that will guide you through the process. Follow these steps:

1. Navigate to the root directory of your project.

2. Run the setup script:

   - **Bash (Linux/macOS)**: Execute `./setup_secrets.sh`
   - **PowerShell (Windows)**: Execute `.\setup_secrets.ps1`

3. The script will prompt you for the necessary information, such as usernames and passwords. Please provide accurate and secure values when prompted.

4. After completing the setup, the script will generate the `secrets.json` and `secrets.tfvars` files in the appropriate location (`azure-tokyo/terraform/`).

Once you've successfully run the setup script and generated the secret files, you can proceed with deploying your Azure infrastructure using Terraform.
