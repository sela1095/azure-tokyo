# Prompt the user for sensitive information
$tokyoWebDevUsername = Read-Host "Enter Tokyo Web Dev Username"
$tokyoWebDevPassword = Read-Host "Enter Tokyo Web Dev Password" -AsSecureString
$tokyoDbDevUsername = Read-Host "Enter Tokyo DB Dev Username"
$tokyoDbDevPassword = Read-Host "Enter Tokyo DB Dev Password" -AsSecureString
$dbUser = Read-Host "Enter DB User"
$dbPassword = Read-Host "Enter DB Password" -AsSecureString

# Convert secure strings to plain text
$tokyoWebDevPasswordPlainText = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($tokyoWebDevPassword))
$tokyoDbDevPasswordPlainText = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($tokyoDbDevPassword))
$dbPasswordPlainText = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($dbPassword))

# Create secrets.json file
$jsonContent = @"
{
    "db_user": "$dbUser",
    "db_password": "$dbPasswordPlainText"
}
"@

$jsonContent | Set-Content -Path "..\terraform\secrets.json" -Force

# Create secrets.tfvars file
$tfvarsContent = @"
tokyo_web_dev_username = "$tokyoWebDevUsername"
tokyo_web_dev_password = "$tokyoWebDevPasswordPlainText"
tokyo_db_dev_username = "$tokyoDbDevUsername"
tokyo_db_dev_password = "$tokyoDbDevPasswordPlainText"
"@

$tfvarsContent | Set-Content -Path "..\terraform\secrets.tfvars" -Force
