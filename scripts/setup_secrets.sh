#!/bin/bash

# Prompt the user for sensitive information
read -p "Enter Tokyo Web Dev Username: " tokyo_web_dev_username
read -s -p "Enter Tokyo Web Dev Password: " tokyo_web_dev_password
echo # Add a newline after the password prompt
read -p "Enter Tokyo DB Dev Username: " tokyo_db_dev_username
read -s -p "Enter Tokyo DB Dev Password: " tokyo_db_dev_password
echo # Add a newline after the password prompt
read -p "Enter DB User: " db_user
read -s -p "Enter DB Password: " db_password
echo # Add a newline after the password prompt

# Create secrets.json file
cat <<EOL > ../terraform/secrets.json
{
    "db_user": "$db_user",
    "db_password": "$db_password"
}
EOL

# Create secrets.tfvars file
cat <<EOL > ../terraform/secrets.tfvars
tokyo_web_dev_username = "$tokyo_web_dev_username"
tokyo_web_dev_password = "$tokyo_web_dev_password"
tokyo_db_dev_username = "$tokyo_db_dev_username"
tokyo_db_dev_password = "$tokyo_db_dev_password"
EOL

echo "Secret files (secrets.json and secrets.tfvars) created successfully."
