#!/bin/bash
sudo apt install -y postgresql jq

json_file="secrets.json"

# Function to retrieve a value by key from JSON
get_secret_value() {
  key="$1"
  jq -r ".$key" "$json_file"
}

# Example usage:
db_user=$(get_secret_value "db_user")
db_password=$(get_secret_value "db_password")

# Use the retrieved values
if [ -n "$db_user" ]; then
   if [ -n "$db_password" ]; then
        cp secrets.json ~postgres/secrets.json
	rm secrets.json
        cd ~postgres/
        echo "listen_addresses = '*'" | sudo tee -a /etc/postgresql/*/main/postgresql.conf
        echo "host    tokyodb         $db_user       all                     scram-sha-256" | sudo tee -a /etc/postgresql/*/main/pg_hba.conf
        sudo systemctl restart postgresql.service
        sudo -u postgres psql -c "ALTER USER postgres with encrypted password '$db_password';"
        #sudo -u postgres psql -c ALTER DATABASE template1 is_template false;"
        #sudo -u postgres psql -c DROP DATABASE template1;"
        sudo -u postgres createuser "$db_user"
        sudo -u postgres psql -c "ALTER USER $db_user with encrypted password '$db_password';"
        sudo -u postgres psql -c "CREATE DATABASE tokyodb;"
        sudo -u postgres psql -c "grant all privileges on database tokyodb to $db_user;"
	sudo -u postgres psql -d tokyodb -c "CREATE TABLE data (id serial PRIMARY KEY, name VARCHAR NOT NULL, value INTEGER NOT NULL, time TIMESTAMP NOT NULL)"
	sudo -u postgres psql -d tokyodb -c "GRANT ALL PRIVILEGES ON TABLE data TO tokyouser;"
	sudo -u postgres psql -d tokyodb -c "INSERT INTO data (name, value, time) VALUES ('John Doe', 42, '2023-09-06 14:30:00')"

        rm ~postgres/secrets.json
   else
       echo "DB Password not found in JSON file"
   fi

else
   echo "DB Username not found in JSON file"
fi
