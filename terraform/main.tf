# Define the Azure Resource Group
resource "azurerm_resource_group" "rg_tokyo_terraform" {
  name     = var.rg_name
  location = var.rg_location
}

# Create Network Security Groups
resource "azurerm_network_security_group" "nsg_tokyo_db" {
  name                = var.nsg_tokyo_db_name
  location            = azurerm_resource_group.rg_tokyo_terraform.location
  resource_group_name = azurerm_resource_group.rg_tokyo_terraform.name
}

resource "azurerm_network_security_group" "nsg_tokyo_web" {
  name                = var.nsg_tokyo_web_name
  location            = azurerm_resource_group.rg_tokyo_terraform.location
  resource_group_name = azurerm_resource_group.rg_tokyo_terraform.name
}

# Create Network Security Group Rules
resource "azurerm_network_security_rule" "nsgsr_tokyo_db_dev_postgresql" {
  name                        = "AllowCidrBlockCustom5432Inbound"
  priority                    = 120
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "5432"
  source_address_prefix       = "10.0.0.0/16"
  destination_address_prefix  = "10.2.0.0/16"
  network_security_group_name = azurerm_network_security_group.nsg_tokyo_db.name
  resource_group_name         = azurerm_resource_group.rg_tokyo_terraform.name
}

# Create nsg_tokyo_db rule
resource "azurerm_network_security_rule" "nsgsr_tokyo_db_dev_SSH" {
  name                        = "AllowMyIpAddressCustom22Inbound"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Deny"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       =  data.http.my_public_ip.response_body
  destination_address_prefix  = "*"
  network_security_group_name = azurerm_network_security_group.nsg_tokyo_db.name
  resource_group_name         = azurerm_resource_group.rg_tokyo_terraform.name
}

output "my_ip_address" {
  value = azurerm_network_security_rule.nsgsr_tokyo_db_dev_SSH.source_address_prefixes
}

# Create nsg_tokyo_web rule
resource "azurerm_network_security_rule" "nsgsr_tokyo_web_dev_HTTP" {
  name                        = "AllowAnyCustomWeb"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  source_address_prefix       = "*"
  destination_port_ranges     = ["80", "8080", "5000"]
  destination_address_prefix  = "*"
  network_security_group_name = azurerm_network_security_group.nsg_tokyo_web.name
  resource_group_name         = azurerm_resource_group.rg_tokyo_terraform.name
}

# Create nsg_tokyo_web rule
resource "azurerm_network_security_rule" "nsgsr_tokyo_web_dev_SSH" {
  name                        = "AllowMyIpAddressSSHInbound"
  priority                    = 110
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       =  data.http.my_public_ip.response_body
  destination_address_prefix  = "*"
  network_security_group_name = azurerm_network_security_group.nsg_tokyo_web.name
  resource_group_name         = azurerm_resource_group.rg_tokyo_terraform.name
}

# Create Virtual Network
resource "azurerm_virtual_network" "vnet_tokyo_dev_westeu" {
  resource_group_name = azurerm_resource_group.rg_tokyo_terraform.name
  name                = var.vnet_name1_dev
  location            = azurerm_resource_group.rg_tokyo_terraform.location
  address_space       = ["10.0.0.0/16", "10.2.0.0/16"]
}

# Create Subnets
resource "azurerm_subnet" "snet_tokyo_db_dev" {
  name                 = var.snet_db
  resource_group_name = azurerm_resource_group.rg_tokyo_terraform.name
  virtual_network_name = azurerm_virtual_network.vnet_tokyo_dev_westeu.name
  address_prefixes     = ["10.2.0.0/16"]
}

resource "azurerm_subnet" "snet_tokyo_web_dev" {
  name                 = var.snet_web
  resource_group_name = azurerm_resource_group.rg_tokyo_terraform.name
  virtual_network_name = azurerm_virtual_network.vnet_tokyo_dev_westeu.name
  address_prefixes     = ["10.0.0.0/16"]
}

resource "azurerm_subnet_network_security_group_association" "nsga_tokyo_web_dev" {
  subnet_id = azurerm_subnet.snet_tokyo_web_dev.id
  network_security_group_id = azurerm_network_security_group.nsg_tokyo_web.id
}

resource "azurerm_subnet_network_security_group_association" "nsga_tokyo_db_dev" {
  subnet_id = azurerm_subnet.snet_tokyo_db_dev.id
  network_security_group_id = azurerm_network_security_group.nsg_tokyo_db.id
}

# Create Public IPs
resource "azurerm_public_ip" "pip_tokyo_web_dev" {
  name                = var.pip_web1_dev
  location            = azurerm_resource_group.rg_tokyo_terraform.location
  resource_group_name = azurerm_resource_group.rg_tokyo_terraform.name
  allocation_method   = "Dynamic"
  sku                 = "Basic"
}


resource "azurerm_public_ip" "pip_tokyo_db_dev" {
  name                = var.pip_db1_dev
  location            = azurerm_resource_group.rg_tokyo_terraform.location
  resource_group_name = azurerm_resource_group.rg_tokyo_terraform.name
  allocation_method   = "Dynamic"
  sku                 = "Basic"
}

resource "azurerm_network_interface" "nic_tokyo_web_dev" {
  name                = var.nic_web1_dev
  location            = azurerm_resource_group.rg_tokyo_terraform.location
  resource_group_name = azurerm_resource_group.rg_tokyo_terraform.name

  ip_configuration {
    name                          = var.ipc_web1_dev
    subnet_id                     = azurerm_subnet.snet_tokyo_web_dev.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id		      = azurerm_public_ip.pip_tokyo_web_dev.id
  }
}

resource "azurerm_network_interface" "nic_tokyo_db_dev" {
  name                = var.nic_db1_dev
  location            = azurerm_resource_group.rg_tokyo_terraform.location
  resource_group_name = azurerm_resource_group.rg_tokyo_terraform.name

  ip_configuration {
    name                          = var.ipc_db1_dev
    subnet_id                     = azurerm_subnet.snet_tokyo_db_dev.id
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.2.0.4"
    public_ip_address_id		      = azurerm_public_ip.pip_tokyo_db_dev.id
  }
}


# =========================================== DB-VM ===========================================
resource "azurerm_linux_virtual_machine" "vm_tokyo_db_dev" {

 admin_ssh_key {
    username   = var.tokyo_db_dev_username
    public_key = file("${var.ssh_public_key_path}")
  }

  name                  = var.vm_db1_dev
  location              = azurerm_resource_group.rg_tokyo_terraform.location
  resource_group_name   = azurerm_resource_group.rg_tokyo_terraform.name
  network_interface_ids = [azurerm_network_interface.nic_tokyo_db_dev.id]
  size                  = "Standard_B2s"
  computer_name         = "vm-tokyo-db"
  admin_username        = var.tokyo_db_dev_username
  admin_password        = var.tokyo_db_dev_password

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts-gen2"
    version   = "latest"
  }

  os_disk {
    caching             = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
}

# create db vm managed disk
resource "azurerm_managed_disk" "sdisk_tokyo_db_dev" {
  name                 = "${azurerm_linux_virtual_machine.vm_tokyo_db_dev.name}-disk1"
  location             = azurerm_resource_group.rg_tokyo_terraform.location
  resource_group_name  = azurerm_resource_group.rg_tokyo_terraform.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 4
}
# attach db disk to db vm
resource "azurerm_virtual_machine_data_disk_attachment" "disk_attach_tokyo_db_dev" {
  managed_disk_id    = azurerm_managed_disk.sdisk_tokyo_db_dev.id
  virtual_machine_id = azurerm_linux_virtual_machine.vm_tokyo_db_dev.id
  lun                = "4"
  caching            = "ReadWrite"
}

resource "null_resource" "remote_execution_tokyo_db" { 
    connection {
      type     = "ssh"
      user     = var.tokyo_db_dev_username
      private_key = file("${var.ssh_private_key_path}")
      host     = azurerm_linux_virtual_machine.vm_tokyo_db_dev.public_ip_address
    }
}

# =========================================== WEB-VM =========================================== 

resource "azurerm_linux_virtual_machine" "vm_tokyo_web_dev" {
 
  admin_ssh_key {
    username   = var.tokyo_web_dev_username
    public_key = file("${var.ssh_public_key_path}")
  }

  name                  = var.vm_web1_dev
  location              = azurerm_resource_group.rg_tokyo_terraform.location
  resource_group_name   = azurerm_resource_group.rg_tokyo_terraform.name
  network_interface_ids = [azurerm_network_interface.nic_tokyo_web_dev.id]
  size                  = "Standard_B1s"
  computer_name         = "vm-tokyo-web"
  admin_username        = var.tokyo_web_dev_username
  admin_password        = var.tokyo_web_dev_password

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts-gen2"
    version   = "latest"
  }

  os_disk {
    caching             = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
}

output "public_web_ip" {
  value = "${azurerm_linux_virtual_machine.vm_tokyo_web_dev.public_ip_address}"
}

output "public_db_ip" {
  value = "${azurerm_linux_virtual_machine.vm_tokyo_db_dev.public_ip_address}"
}

resource "null_resource" "remote_execution_tokyo_web" { 

    connection {
      type     = "ssh"
      user     = var.tokyo_web_dev_username
      private_key = file("${var.ssh_private_key_path}")
      host     = azurerm_linux_virtual_machine.vm_tokyo_web_dev.public_ip_address
    }

    provisioner "file" {
      source = var.ssh_private_key_path
      destination = "./.ssh/id_rsa"
    }

    provisioner "file" {
      source = var.ssh_public_key_path
      destination = "./.ssh/id_rsa.pub"
    }

    provisioner "file" {
      source = "./secrets.json"
      destination = "./secrets.json"
    }

    provisioner "remote-exec" {
    inline = [
      "touch myWebFile",
      "cd .ssh",
      "sudo chmod 600 id_rsa",
      "sudo chmod 600 id_rsa.pub",
      "sudo chmod 755 ~/.ssh",
      "cd ..",
      "sudo apt update",
      "sudo apt install -y git",
      "git clone https://gitlab.com/sela1095/azure-tokyo.git",

      "touch disk_mount.sh",   
      "echo \"sudo mkfs -t ext4 /dev/sdc\" >> disk_mount.sh",
      "echo \"sudo mkdir /data1\" >> disk_mount.sh",
      "echo \"sudo mount /dev/sdc /data1\" >> disk_mount.sh",
      "echo \"sudo apt update\" >> disk_mount.sh",
      "echo \"sudo apt install -y git\" >> disk_mount.sh",

      "scp -o  StrictHostKeyChecking=no ./disk_mount.sh 10.2.0.4:~/disk_mount.sh",
      "scp -o  StrictHostKeyChecking=no ./secrets.json 10.2.0.4:~/secrets.json",
      "scp -o  StrictHostKeyChecking=no ./azure-tokyo/scripts/setup_db.sh 10.2.0.4:~/setup_db.sh",

      "rm ./disk_mount.sh",
      "ssh -o StrictHostKeyChecking=no 10.2.0.4 sudo chmod +x disk_mount.sh",
      "ssh 10.2.0.4 sudo ./disk_mount.sh",
      "ssh 10.2.0.4 sudo chmod +x setup_db.sh",
      "ssh 10.2.0.4 sudo ./setup_db.sh",
      "cd ./azure-tokyo/scripts",
      "mv ../../secrets.json .",
      "chmod +x setup_webapp.sh run_webapp.sh tokyo-webapp.py",
      "bash setup_webapp.sh",
    ]
  }
  depends_on = [ azurerm_virtual_machine_data_disk_attachment.disk_attach_tokyo_db_dev ]
}



