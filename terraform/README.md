# Azure Tokyo Terraform Configuration

This repository contains the Terraform configuration for deploying resources in Azure Tokyo. Follow the instructions below to get started.

## Prerequisites

1. Ensure that you have the Azure CLI and Terraform installed and properly configured on your system. If you haven't done so already, you can refer to the official documentation for guidance: [Azure Terraform Quickstart](https://learn.microsoft.com/en-us/azure/developer/terraform/quickstart-configure).
Note: You might need to use **`terraform init -upgrade`** command as part of your configuration.

## Setup

1. Create the following files in your project directory (`azure-tokyo/terraform/`):

   - `secrets.tfvars`
   - `secrets.json`

2. In `secrets.tfvars`, include the following variables:

   ```hcl
   tokyo_web_dev_username = "username"
   tokyo_web_dev_password = "password"
   tokyo_db_dev_username = "username"
   tokyo_db_dev_password = "password"

Make sure to replace "username" and "password" with the actual credentials.

1. In secrets.json, include the following JSON object:
   ```hcl
   {    
      "db_user": "username",
      "db_password": "password"
   }
    
Again, replace "username" and "password" with the actual database credentials.

Note: The secrets.tfvars file is optional, while the secrets.json file is mandatory.

Ensure that both of these files are placed in the project's directory under azure-tokyo/terraform/.

## Deploying Resources

To deploy your resources in Azure Tokyo, you can use Terraform. Run the following command:
**`terraform apply -var-file="secrets.tfvars" -auto-approve`**

`-var-file="secrets.tfvars":` This flag specifies the variable file (`secrets.tfvars`) containing sensitive information like credentials. It allows you to separate sensitive data from your code.

`-auto-approve`: This flag automatically approves and applies the changes without requiring manual confirmation, which is useful for automation scripts.

Make sure you've configured the necessary variables in secrets.tfvars before running this command.

Now you are ready to use Terraform to provision resources in Azure Tokyo. Follow the appropriate Terraform commands to deploy your infrastructure.

Happy coding!
