# Resouce Groups
variable "rg_name" {
    default =  "rg_tokyo_terraform"
}

variable "rg_location" {
    default =  "West Europe"
}


# Network Security Groups (NSGs)
variable "nsg_tokyo_db_name" {
  default = "nsg-tokyo-db"
}

variable "nsg_tokyo_web_name" {
  default = "nsg-tokyo-web"
}

# Virtual Networks (Vnets)
variable "vnet_name1_dev" {
  default = "vnet-tokyo-dev-westeu"
}

# Subnets
variable "snet_db" {
    default = "snet-tokyo-db"
}

variable "snet_web" {
    default = "snet-tokyo-web"
}

# Public IPs (PIPs)
variable "pip_web1_dev" {
  default = "pip_tokyo_web_dev"
}

variable "pip_db1_dev" {
  default = "pip_tokyo_db_dev"
}

# Network Interface Cards (NICs)
variable "nic_web1_dev" {
  default = "nic_tokyo_web_dev"
}

variable "nic_db1_dev" {
  default = "nic_tokyo_db_dev"
}

# IP Configurations (IPCs)
variable "ipc_web1_dev" {
  default = "ipc_tokyo_web_dev"
}

variable "ipc_db1_dev" {
  default = "ipc_tokyo_db_dev"
}

# Virtual Machine(VMs)
variable "vm_web1_dev" {
  default = "vm_tokyo_web_dev"
}
variable "vm_db1_dev" {
  default = "vm_tokyo_db_dev"
}