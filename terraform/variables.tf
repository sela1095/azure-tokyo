variable "ssh_public_key_path" {
  default = "~/.ssh/id_rsa.pub"
}

variable "ssh_private_key_path" {
  default = "~/.ssh/id_rsa"
}

variable "my_subscription_id" {
  default = "78f3d732-b1e0-4873-b03c-34c2e23adeb1"
}

variable "tokyo_web_dev_username" {
  description = "your web username"
  type = string
}

variable "tokyo_web_dev_password" {
  description = "your web password"
  type = string
}

variable "tokyo_db_dev_username" {
  description = "your db username"
  type = string
}

variable "tokyo_db_dev_password" {
  description = "your db username"
  type = string
}
