# Azure Infrastructure Deployment with Terraform

This GitLab repository is dedicated to the deployment of Azure infrastructure using Terraform. The project's primary objective is to securely provision resources for a web application named "Tokyo" and a PostgreSQL database.

## Project Overview

### Security Measures

- **Sensitive Data Protection**: This project emphasizes security by securely managing sensitive information such as passwords and variables. Two key security practices include:
  - Utilizing a `secrets.json` file to store sensitive project-related data.
  - Employing `secrets.tfvars` for securely managing Terraform variables, ensuring that sensitive information remains confidential.

### Folder Structure

The repository is organized into two primary folders, each serving a distinct purpose:

- **`scripts` Folder**: This folder houses a collection of scripts that play a crucial role in the provisioning and configuration of resources within the VMs. These scripts are integral to setting up and maintaining the infrastructure components.

- **`terraform` Folder**: Within this folder, you will find the Terraform configuration files. These files define and deploy the Azure infrastructure, including the creation of the "Tokyo" web application VM and the PostgreSQL database VM. The configuration also ensures the secure network segregation of resources.

## Purpose of the Project

The primary purpose of this project is to streamline the deployment of Azure infrastructure in a secure and efficient manner. By leveraging Terraform, the project aims to achieve the following:

- **Automation**: Automate the provisioning and management of resources to reduce manual configuration tasks.

- **Security**: Emphasize security measures such as secure password management, network security group rules, and secure data access controls.

- **Scalability**: Design infrastructure in a way that allows for easy scalability as the application grows.

- **Maintainability**: Facilitate ease of maintenance and updates through infrastructure as code.

By adhering to these principles, this project seeks to create a reliable and secure foundation for hosting the "Tokyo" web application and PostgreSQL database on Azure.

For detailed deployment and usage instructions, refer to specific documentation within the respective folders.

Feel free to explore the contents of the `scripts` and `terraform` folders for in-depth details on the project's implementation and configuration.

## Usage
1. Navigate to **`./scripts` Folder** and run **`setup_secrets`** script. **`.sh`** is for linux and **`.ps1`** is for windows (powershell)
2. Navigate to **`./terraform` Folder** and make sure to follow **`README.md`**
